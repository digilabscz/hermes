<?php declare(strict_types=1);

namespace Digilabscz\Hermes;

use Composer\Composer;
use Composer\EventDispatcher\EventSubscriberInterface;
use Composer\IO\IOInterface;
use Composer\Plugin\PluginInterface;
use Composer\Script\Event;
use Composer\Script\ScriptEvents;
use RuntimeException;

final class Plugin implements PluginInterface, EventSubscriberInterface
{
    public function activate(Composer $composer, IOInterface $io): void
    {
        self::runSync($composer, $io);
    }

    public function deactivate(Composer $composer, IOInterface $io): void
    {
        self::runSync($composer, $io);
    }

    public function uninstall(Composer $composer, IOInterface $io): void
    {
        self::runClear($composer, $io);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            ScriptEvents::POST_AUTOLOAD_DUMP => 'sync',
        ];
    }

    public static function sync(Event $event): void
    {
        $io = $event->getIO();
        if ($io->isInteractive()) {
            return;
        }

        self::runSync($event->getComposer(), $io);
    }

    public static function clear(Event $event): void
    {
        self::runClear($event->getComposer(), $event->getIO());
    }

    private static function runSync(Composer $composer, IOInterface $io): void
    {
        (new Mapper($composer, $io))->sync(
            self::getAssetDir($composer, $io)
        );
    }

    private static function runClear(Composer $composer, IOInterface $io): void
    {
        (new Mapper($composer, $io))->clear(
            self::getAssetDir($composer, $io)
        );
    }

    private static function getAssetDir(Composer $composer, IOInterface $io): string
    {
        $extra = $composer->getPackage()->getExtra();
        if (empty($extra['hermes-assets-dir'])) {
            $io->writeError('<error>Hermes assets directory is not specified in composer.json</error>');
            throw new RuntimeException('Hermes assets directory is not specified in composer.json');
        }

        return $extra['hermes-assets-dir'];
    }
}
