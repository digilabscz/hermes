<?php declare(strict_types=1);

namespace Digilabscz\Hermes;

use Composer\Composer;
use Composer\IO\IOInterface;
use RuntimeException;

final readonly class Mapper
{
    public function __construct(
        private Composer $composer,
        private IOInterface $io,
    ) {}

    public function sync(string $assetsDir): void
    {
        $vendorDir = $this->composer->getConfig()->get('vendor-dir');
        $rootDir = dirname($vendorDir);

        $mappingPath = $rootDir . '/' . trim($assetsDir, '/') . '/vendor/';
        $this->deleteDir($mappingPath);

        foreach ($this->composer->getRepositoryManager()->getLocalRepository()->getPackages() as $package) {
            $packageDir = $vendorDir . '/' . $package->getPrettyName();
            $assetsFile = $packageDir . '/.hermes';

            if (file_exists($assetsFile)) {
                $files = file($assetsFile, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);

                foreach ($files as $file) {
                    $file = ltrim($file, '/');

                    $symlinkPath = $mappingPath . $package->getPrettyName() . '/' . $file;
                    $symlinkDir = dirname($symlinkPath);

                    $originalPath = $packageDir . '/' . $file;

                    if (! file_exists($symlinkDir) && ! mkdir($symlinkDir, 0777, true) && ! is_dir($symlinkDir)) {
                        throw new RuntimeException(sprintf('Directory "%s" was not created', $symlinkDir));
                    }

                    symlink($originalPath, $symlinkPath);
                    $this->io->write('Created symlink: ' . $symlinkPath . ' -> ' . $originalPath);
                }
            }
        }
    }

    public function clear(string $assetsDir): void
    {
        $mappingPath = $assetsDir . '/vendor/';
        $this->deleteDir($mappingPath);
    }

    private function deleteDir(string $dir): void
    {
        if (! file_exists($dir)) {
            return;
        }

        if (! is_dir($dir)) {
            unlink($dir);

            return;
        }

        $items = array_diff(scandir($dir), ['.', '..']);
        foreach ($items as $item) {
            $path = $dir . '/' . $item;
            is_dir($path) ? $this->deleteDir($path) : unlink($path);
        }

        rmdir($dir);
    }
}
