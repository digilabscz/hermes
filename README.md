
# Hermes

Hermes is a Composer plugin that helps you manage asset files by creating symbolic links. This plugin reads a `.hermes` file from your package and maps the specified files to a designated asset directory in your project.

## Installation

To install Hermes, run the following command in your project directory:

```sh
composer require digilabscz/hermes
```

## Configuration

After installing the plugin, you need to configure it in your `composer.json` file. Specify the asset directory where you want the symbolic links to be created using the `extra` section.

```json
{
    "require": {
        "digilabscz/hermes": "*"
    },
    "extra": {
        "hermes-assets-dir": "www/assets"
    }
}
```

## Usage

1. **Define Assets in Packages**: In any Composer package that contains assets you want to be symlinked, create a file named `.hermes` in the root directory of the package. List the paths to the asset files relative to the package root in this file.

    Example `.hermes` file:

    ```plaintext
    /js/datagrid.js
    /css/styles.css
    ```

2. **Install Packages**: When you install or update your Composer dependencies, the plugin will automatically read the `.hermes` files from each package and create the specified symbolic links in the configured asset directory.

3. **Uninstall Packages**: When you uninstall a package, the plugin will remove the symbolic links associated with that package from the asset directory.

## Example

Given the following `.hermes` file in the `digilabscz/datagrid` package:

```plaintext
/js/datagrid.js
```

And the following configuration in your main project's `composer.json`:

```json
{
    "require": {
        "digilabscz/hermes": "*"
    },
    "extra": {
        "hermes-assets-dir": "www/assets"
    }
}
```

After running `composer install`, the plugin will create the following symbolic link:

```plaintext
[ROOT_DIR]/www/assets/vendor/digilabscz/datagrid/js/datagrid.js -> [ROOT_DIR]/vendor/digilabscz/datagrid/js/datagrid.js
```

## Error Handling

If the `hermes-assets-dir` is not specified in the `extra` section of your `composer.json`, the plugin will throw an error and stop execution:

```plaintext
Hermes assets directory is not specified in composer.json
```

Make sure to configure the `hermes-assets-dir` to avoid this error.

## License

This project is licensed under the MIT License.
